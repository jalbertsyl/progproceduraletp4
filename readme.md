# Readme.md
Ce fichier readme.md contient les réponses des exercices du TP ainsi que les instructions pour compiler et executer les programmes du TP.
Ce programme est disponnible sur Gitlab 'https://gitlab.etude.eisti.fr/jalbertsyl/progproceduraletp4'

## Documentation
La documentation est faite avec doxygen. Pour la créer il faut executer la comande suivante :

  doxygen Doxyfile

Puis visualiser dans votre navigateur le fichier ./html/index.html

## Compiler
Pour compiler, il faut saisir dans le terminal linux, à la racine du projet, la commande suivante :

  make

Pour nétoyer les fichiers ".o" créé par la commande make, il suffi d'executer la commande suivante :

  make clean

## Executer
Pour executer, il suffi de saisir dans le terminal linux, à la racine du projet, la commande suivante :

  ./tp4

## Exercices
### 1 - Aire d’un disque par quadrillage
Pour cet exercice veuillez vous référer à la fonction 'float aireDisqueParQuadrillage(int int_n)' défini dans le fichier 'approximationDePi.c'.

### 2 - Madhava de Sangamagrama
Pour cet exercice veuillez vous référer à la fonction 'float madhavaDS(int int_n)' défini dans le fichier 'approximationDePi.c'.

### 3 - John Wallis
Pour cet exercice veuillez vous référer à la fonction 'float johnWallis(int int_n)' défini dans le fichier 'approximationDePi.c'.

### 4 - Isaac Newton
Pour cet exercice veuillez vous référer à la fonction 'float issacNewton(int int_n)' défini dans le fichier 'approximationDeV2.c'.

### 5 - Edmund Halley
Pour cet exercice veuillez vous référer à la fonction 'float edmundHalley(int int_n)' défini dans le fichier 'approximationDeV2.c'.

### 6 - Méthode de Théon de Smyrne
Pour cet exercice veuillez vous référer à la fonction 'float theonDeSmyrne(int int_n)' défini dans le fichier 'approximationDeV2.c'.
