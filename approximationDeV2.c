/*!
\file approximationDeV2.c
\autor Jalbert Sylvain
\version 1
\date 25 octobre 2019
\brief deffinition des fonctions qui vont approximer V2
*/

#include "approximationDeV2.h"

/*!
\fn float issacNewton ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction qui calcul V2 avec la méthode de Issac Newton
\param int_n le nombre d'itération
\return l'appriximation de V2 trouvé par cette méthode
*/
float issacNewton(int int_n){
  //DECLARATION DES VARIABLES
  float float_somme; // le résultat de la suite (uint_n)

  //INITIALISATION DES VARIABLES
  float_somme = 1.00; // u0 = 1

  //CALCUL DE V2
  for(int int_i = int_n ; int_i>0 ; int_i--){ // boucle int_n fois car u0 a déjà été traité
    float_somme = float_somme/2.00 + 1.00/float_somme; //calcul de un+1
  }

  //RETOURNER L'APROXIMATION DE V2 -> le résultat de la suite (uint_n) -> (float_somme)
  return float_somme;
}

/*!
\fn float edmundHalley ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 26 octobre 2019
\brief la fonction qui calcul V2 avec la méthode de Edmund Halley
\param int_n le nombre d'itération
\return l'appriximation de V2 trouvé par cette méthode
*/
float edmundHalley(int int_n){
  //DECLARATION DES VARIABLES
  float float_somme; // la somme de la suite (xint_n)

  //INITIALISATION DES VARIABLES
  float_somme = 1.00; // x0 = 1

  //CALCUL DE V2
  for(int int_i = int_n ; int_i>0 ; int_i--){ // boucle int_n fois car x0 a déjà été traité
    float_somme = float_somme*(float_somme*float_somme+6.00)/(3.00*float_somme*float_somme+2.00); //calcul de xn+1
  }

  //RETOURNER L'APROXIMATION DE V2 -> le résultat de la suite (xint_n) -> (float_somme)
  return float_somme;
}

/*!
\fn float theonDeSmyrne ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 26 octobre 2019
\brief la fonction qui calcul V2 avec la méthode de Théon de Smyrne
\param int_n le nombre d'itération
\return l'appriximation de V2 trouvé par cette méthode
*/
float theonDeSmyrne(int int_n){
  //DECLARATION DES VARIABLES
  float float_p; // est le résultat de la suite (pint_n)
  float float_q; // est le résultat de la suite (qint_n)
  float float_pSave; // sera une sauvegarde temporaire de la suite (pint_n)

  //INITIALISATION DES VARIABLES
  float_p = 1.00; // p0 = 1
  float_q = 1.00; // q0 = 1

  //CALCUL DE V2
  for(int int_i = int_n ; int_i>0 ; int_i--){ // boucle int_n fois car p0 et q0 ont déjà été traité
    float_pSave = float_p; // sauvegarde de pint_n
    float_p = float_p + 2 * float_q; // calcul de pn+1
    float_q = float_pSave + float_q; // calcul de qn+1
  }

  //RETOURNER LA VALEUR DE V2
  return float_p/float_q; // V2 = pn/qn
}
