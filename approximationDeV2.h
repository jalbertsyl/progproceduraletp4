/*!
\file approximationDeV2.h
\autor Jalbert Sylvain
\version 1
\date 25 octobre 2019
\brief le fichier qui contient la déclaration des fonction qui servent à approximer V2
*/

#ifndef __APPROXIMATIONDEV2_H_
#define __APPROXIMATIONDEV2_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*!
\fn float issacNewton ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction qui calcul V2 avec la méthode de Issac Newton
\param int_n le nombre d'itération
\return l'appriximation de V2 trouvé par cette méthode
*/
float issacNewton(int int_n);

/*!
\fn float edmundHalley ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 26 octobre 2019
\brief la fonction qui calcul V2 avec la méthode de Edmund Halley
\param int_n le nombre d'itération
\return l'appriximation de V2 trouvé par cette méthode
*/
float edmundHalley(int int_n);

/*!
\fn float theonDeSmyrne ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 26 octobre 2019
\brief la fonction qui calcul V2 avec la méthode de Théon de Smyrne
\param int_n le nombre d'itération
\return l'appriximation de V2 trouvé par cette méthode
*/
float theonDeSmyrne(int int_n);

#endif
