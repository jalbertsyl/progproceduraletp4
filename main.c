/*!
\file main.c
\autor Jalbert Sylvain
\version 1
\date 22 octobre 2019
\brief un programme qui va proposer d'aproximer pi ou la racine carré de 2
*/

#include "approximationDePi.h"
#include "approximationDeV2.h"

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction principale qui va se charger d'afficher l'approximation de pi ou de la racine carré de 2, selon le choix de l'utilisateur
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //DECLARATION DES VARIABLES
  int int_choix; //est le futur choix de l'utilisateur
  int int_param; //est le futur parametre de la fonction qui sera appelé

  //AFFICHAGE DU MENU
  printf("Ce programme peut approximer la valeur de pi par la méthode :\n\t1 - Aire d\'un disque par quadrillage\n\t2 - Madhava de Sangamagrama\n\t3 - John Wallis\nCe programme peut aussi approximer la valeur de V2 par la méthode :\n\t4 - Issac Newton\n\t5 - Edmund Halley\n\t6 - Théon de Smyrne\n");

  //CHOIX DE L'UTILISATEUR
  printf("Veullez entrer l'entier qui correspond à votre choix : ");
  if(!scanf("%d", &int_choix)){ //lecture du choix de l'utilisateur. L'entier sera dans la variable int_choix
    //si le choix entré n'est pas un entier alors on quitte le programme
    printf("La valeur saisie n'est pas un entier !\n");
    exit(-1);
  }

  switch (int_choix) {
    case 1: //L'utilisateur à choisi l'Aire d’un disque par quadrillage
      printf("Aire d\'un disque par quadrillage\n"); // On informe la méthode que l'utilisateur a choisie
      //SAISIE DU NOMBRE DE POINT
      printf("Entrer le nombre de point à générer : "); // Demande de saisie du nombre de points
      if(!scanf("%d", &int_param)){ //saisie du paramètre, s'il n'est pas un entier alors on quitte le programme
        printf("La valeur saisie n'est pas un entier !\n");
        exit(-1);
      }
      //AFFICHAGE DU RESULTAT
      printf("\tPi est égal à : %f\n", aireDisqueParQuadrillage((int_param<0 ? 0-int_param : int_param))); // le paramètre donné à la méthode est positif
      break;

    case 2: //L'utilisateur a choisi la méthode de Madhava de Sangamagrama
      printf("Madhava de Sangamagrama\n");
      //SAISIE DU NOMBRE D'ITERATION
      printf("Entrer le nombre d'itération : "); // Demande de saisie du nombre d'itération
      if(!scanf("%d", &int_param)){ //saisie du paramètre, s'il n'est pas un entier alors on quitte le programme
        printf("La valeur saisie n'est pas un entier !\n");
        exit(-1);
      }
      //AFFICHAGE DU RESULTAT
      printf("\tPi est égal à : %f\n", madhavaDS((int_param<0 ? 0-int_param : int_param))); // le paramètre donné à la méthode est positif
      break;

    case 3: //L'utilisateur a choisi la méthode de John Wallis
      printf("John Wallis\n");
      //SAISIE DU NOMBRE D'ITERATION
      printf("Entrer le nombre d'itération : ");
      if(!scanf("%d", &int_param)){ //saisie du paramètre, s'il n'est pas un entier alors on quitte le programme
        printf("La valeur saisie n'est pas un entier !\n");
        exit(-1);
      }
      //AFFICHAGE DU RESULTAT
      printf("\tPi est égal à : %f\n", johnWallis((int_param<0 ? 0-int_param : int_param))); // le paramètre donné à la méthode est positif
      break;

    case 4: //L'utilisateur a choisi la méthode de Issac Newton
      printf("Issac Newton\n");
      //SAISIE DU NOMBRE DE POINT
      printf("Entrer le nombre d'itération : ");
      if(!scanf("%d", &int_param)){ //saisie du paramètre, s'il n'est pas un entier alors on quitte le programme
        printf("La valeur saisie n'est pas un entier !\n");
        exit(-1);
      }
      //AFFICHAGE DU RESULTAT
      printf("\tV2 est égal à : %f\n", issacNewton((int_param<0 ? 0-int_param : int_param))); // le paramètre donné à la méthode est positif
      break;

    case 5: //L'utilisateur a choisi la méthode de Edmund Halley
      printf("Edmund Halley\n");
      //SAISIE DU NOMBRE D'ITERATION
      printf("Entrer le nombre d'itération : ");
      if(!scanf("%d", &int_param)){ //saisie du paramètre, s'il n'est pas un entier alors on quitte le programme
        printf("La valeur saisie n'est pas un entier !\n");
        exit(-1);
      }
      //AFFICHAGE DU RESULTAT
      printf("\tV2 est égal à : %f\n", edmundHalley((int_param<0 ? 0-int_param : int_param))); // le paramètre donné à la méthode est positif
      break;

    case 6: //L'utilisateur a choisi la méthode de Théon de Smyrne
      printf("Théon de Smyrne\n");
      //SAISIE DU NOMBRE D'ITERATION
      printf("Entrer le nombre d'itération : ");
      if(!scanf("%d", &int_param)){ //saisie du paramètre, s'il n'est pas un entier alors on quitte le programme
        printf("La valeur saisie n'est pas un entier !\n");
        exit(-1);
      }
      //AFFICHAGE DU RESULTAT
      printf("\tV2 est égal à : %f\n", theonDeSmyrne((int_param<0 ? 0-int_param : int_param))); // le paramètre donné à la méthode est positif
      break;

    default: //L'utilisateur a entrée une autre valeur, il a donc choisi de ne rien faire
      printf("Vous avez choisi de ne rien faire. Au revoir !\n");
      break;
  }

  /* Fin du programme, Il se termine normalement, et donc retourne 0 */
  return 0;
}
