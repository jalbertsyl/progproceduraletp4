/*!
\file approximationDePi.c
\autor Jalbert Sylvain
\version 1
\date 22 octobre 2019
\brief deffinition des fonctions qui vont approximer pi
*/

#include "approximationDePi.h"

/*!
\fn float aireDisqueParQuadrillage ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction qui calcul l'air d'un disque par quadrillage, afin d'approximer pi
\param int_n le nombre de point générer
\return l'appriximation de pi trouvé par cette méthode
*/
float aireDisqueParQuadrillage(int int_n){
  //DECLARATION DES VARIABLES
  float float_air; //l'air du cercle, sera la valeur cherché
  float float_r; //rayon du cercle, sera une valeur connu dès
  int int_compteur; //compteur pour savoir combien de points sont dans le cercle
  float float_x;  float float_y; //les coordonnées du points qui sera généré de facon aléatoire

  //INITIALISATION DES VARIABLES
  float_r = 1.00; //Initialisation du rayon
  int_compteur = 0; //Initialisation du compteur à 0 car au début 0 point sont dans le cercle

  //CALCUL DE L'AIR
  for (int i = 0 ; i<int_n ; i++){
    //GENERER UN POINT
    float_x = rand()/(float)(RAND_MAX) * float_r; // float_x prend un nombre aléatoire entre 0 et float_r (le rayon)
    float_y = rand()/(float)(RAND_MAX) * float_r; // float_y prend un nombre aléatoire entre 0 et float_r (le rayon)
    //VERIFIER SI LE POINT SE TROUVE DANS LE QUART DE CERCLE
    if(sqrtf((float_x*float_x)+(float_y*float_y)) <= float_r){
      int_compteur++; // si le point est dans l'arc de cercle, on incremente le compteur
    }
  }
  float_air = (float_r*float_r*int_compteur/int_n)*4.00; // l'air est égal à rayon au carré fois le ratio (nombre de points dans quart de cerle / nombre de point total) fois 4 pour avoir l'air total du cercle

  //RETOURNER L'APROXIMATION DE PI
  return (float_air/(float_r*float_r)); // PI = AIR / RAYON²
}

/*!
\fn float madhavaDS ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction qui calcul pi avec la formule mathématique de Madhava de Sangamagrama
\return l'appriximation de pi trouvé par cette formule
*/
float madhavaDS(int int_n){
  //DECLARATION DES VARIABLES
  float float_somme; // le résultat de la somme au rang int_n

  //INITIALISATION DES VARIABLES
  float_somme = 0.00; // Initialisation de la somme

  //CALCUL DE LA SOMME (au rang int_n)
  for(int int_i = int_n ; int_i>=0 ; int_i--){ // int_n sera décrémenté à chaque ittération, jusqu'a 0 inclu
    float_somme += (int_i%2==0 ? 1.00 : -1.00)/(2.00*(float)int_i+1.00); //calcul du rang int_n courant de la somme
  }

  //RETOURNER L'APROXIMATION DE PI
  return 4.00*float_somme; // PI est environ égale à 4 fois la somme au rang int_n
}

/*!
\fn float johnWallis ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief la fonction qui calcul pi avec la formule mathématique de John Wallis
\param int_n le nombre d'itération
\return l'appriximation de pi trouvé par cette formule
*/
float johnWallis(int int_n){
  //DECLARATION DES VARIABLES
  float float_produit; // le résultat du produit au rang int_n

  //INITIALISATION DES VARIABLES
  float_produit = 1.00; // Initialisation du produit

  //CALCUL DE PI
  for(int int_i = int_n ; int_i>=1 ; int_i--){ // int_n sera décrémenté à chaque ittération, jusqu'a 1 inclu
    float_produit = float_produit * 4.00*(float)int_i*(float)int_i/(4.00*(float)int_i*(float)int_i-1.00); //calcul du rang int_n courant du produit
  }

  //RETOURNER L'APROXIMATION DE PI
  return 2.00*float_produit;
}
