#File Makefile
#Autor Sylvain Jalbert
#Date 27 octobre 2019
#Resum le fichier de configuration de la commande make, qui va sert ici à compiler de manière optimisé tout le projet

CC = gcc -Wall
RM = rm -f
SRC = $(wildcard *.c)
HEAD = $(wildcard *.h)
OBJ = $(SRC:.c=.o)
PROG = tp4

all : $(PROG)

$(PROG) : $(OBJ) # compilation du programme
	gcc $^ -o $@ -lm

approximationDeV2.o : approximationDeV2.h

approximationDePi.o : approximationDePi.h

.c.o :					 # compilation des objets
	$(CC) -c $< -o $@

.PHONY : clean

clean :
	$(RM) $(OBJ)
